# Megajudge3000 controller

## Getting started

Follow these steps to get a local development version of the Controller running:

  - Make sure you have `poetry` and `yarn` installed (if not, install them in a way appropriate for
    your distribution or ...).
  - Install node.js through [NVM](https://github.com/nvm-sh/nvm) and you can afterwards
    install the current development version of node for this project with `nvm use` command ran in
    the `frontend/` folder. After node is installed, you can install `yarn` with `npm install -g yarn`.
  - Run `poetry install` to install Python dependencies of the backend.
  - Run `poetry run pre-commit install` to install a pre-commit hook. This will run some quick
    checks and fixes on the code before it is committed.
  - Apply migrations to your local development database - `poetry run ./manage.py migrate`.
    **You will need to do this every time you pull new changes that contain migrations**.
  - Start Django development server - `poetry run ./manage.py runserver`

  - In another terminal, go to folder `frontend/` and run `yarn install` there, to install frontend
    dependencies.
  - Run `yarn dev` to start frontend development server. You can then access the app on
    `http://localhost:3000`. The Django server needs to be running for this to work. Otherwise, you
    will need to dump the GraphQL schema and run `yarn generate-types-production`.

Note: As stated in the Create React App docs, the development proxy will only forward requests
without `text/html` in the `Accept` header. This means that you won't be able to access pages
rendered by Django such as Django Admin over this proxy. To access it, use `http://localhost:8000`.

### Production settings in Docker container

See `controller_backend/settings/production.py` for what environment variables you'll need to set.
If you need to customize any other settings in production, bind-mount your custom configuration file
to `/etc/m3k/settings.py` in the Docker container.


## controller_backend Django project structure

We wanted to be able to build our Django app into a wheel in order to be able
to easily install it into a docker image to standard location. This gives us
the benefits of using standard Python locations for everything and not needing
to edit `PYTHONPATH` and other environment variables.

We are using project structure which differs from the traditional Django
structure in the following ways:

- root contains root `urls.py` for the whole project and folders `settings` and `apps`
- `settings` folder contains `base.py` with settings which are common to both production and development.
  This file is imported in both `development.py` and `production.py`, which are then used by their
  respective environments.
- `apps` folder contains all Django apps in the project


### `jobs` app

This app implements a job distribution system and API for use by the workers.

Note: This app could benefit from using Postgres-specific JSONField and ArrayField. We've opted not
to use them so that SQLite may still be used for local development. If this causes problems, or if
you don't consider using SQLite for local development advantageous any more, feel free to rewrite
it.

### Running tests on PostgreSQL

By default, tests run on an sqlite database. This DB won't be used in production, so it is a good
idea to run tests on a production database as well. With postgres and docker you can do this simply:

```
# In another terminal, run
docker run --rm -p 5432:5432 -e POSTGRES_PASSWORD="djangotests" -e POSTGRES_USER="djangotests" \
    postgres:12.2-alpine`
```

Then before running your tests just set the following environment variables:

  - `DATABASE_ENGINE='django.db.backends.postgresql`
  - `DATABASE_NAME='controller_backend`
  - `DATABASE_USER='djangotests'`
  - `DATABASE_PASSWORD='djangotests'`

For example by running

```
DATABASE_ENGINE='django.db.backends.postgresql' DATABASE_NAME='controller' \
    DATABASE_USER='djangotests' DATABASE_PASSWORD='djangotests' DATABASE_HOST='127.0.0.1' \
    ./manage.py test
```

## Considerations for production deployment

  - Make sure to use database supported by `select_for_update` Django function. It must also support
    the `skip_locked` attribute. See
    [`select_for_update()` docs](https://docs.djangoproject.com/en/3.0/ref/models/querysets/#select-for-update)
    for supported databases. Otherwise race-conditions may occur when assigning jobs to workers.
    Postgres is fine.
  - Make sure to read and address any warnings shown by `django-admin check --deploy` when starting
    the production Docker container.
