import React, {Dispatch, SetStateAction} from 'react';
import {ApolloProvider} from '@apollo/react-hooks';
import client from './apolloClient';
import MegajudgeDashboard from "./MegajudgeDashboard";
import SignIn from "./SignIn";

type SetUserLoggedIn = Dispatch<SetStateAction<boolean>>
export interface UserContextInterface {
  isUserLoggedIn: boolean,
  setUserLoggedIn: SetUserLoggedIn
}

const defaultUserContext: UserContextInterface = {
  isUserLoggedIn: false,
  setUserLoggedIn: val => {
    console.error("Using default setUserJwt impl in the user context! Did you forget " +
                  "UserContext.Provider?")
  }
}

export const UserContext = React.createContext(defaultUserContext);

function App() {
  const [isUserLoggedIn, setUserLoggedIn] = React.useState(false);
  const userContext = {isUserLoggedIn, setUserLoggedIn} as UserContextInterface
  const apolloClient = client(userContext)

  return (
    <UserContext.Provider value={userContext}>
      <ApolloProvider client={apolloClient}>
        {
          isUserLoggedIn ? <MegajudgeDashboard /> : <SignIn />
        }
      </ApolloProvider>
    </UserContext.Provider>
  );
}

export default App;
