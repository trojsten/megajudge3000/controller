import { gql } from 'apollo-boost';

export default gql`
  fragment Submit on SubmitType {
    id
    priority
    competitionSpecificId
    assetType
    submitAssetsUrl
    submittedAt
    task {
      name
      competition {
        name
      }
    }
  }

  fragment SubmitPage on SubmitPaginatedType {
    page
    pages
    count
    hasNext
    hasPrev
    objects {
      ...Submit
    }
  }

  query SubmitList($rowsPerPage: Int, $page: Int) {
    submits(rowsPerPage: $rowsPerPage, page: $page) {
      ...SubmitPage
    }
  }
`;
