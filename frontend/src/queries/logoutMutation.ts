import { gql } from 'apollo-boost';

export default gql`
  mutation RunLogout {
    logout {
      ok
    }
  }
`;
