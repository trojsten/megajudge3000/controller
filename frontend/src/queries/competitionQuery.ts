import { gql } from 'apollo-boost';

export default gql`
  fragment CompetitionInfo on CompetitionType {
    name
    webhookUrl
  }
  query CompetitionList {
    competitions {
      ...CompetitionInfo
    }
  }
`;
