import { gql } from 'apollo-boost';

export default gql`
  query Me {
    me {
      username,
      email
    }
  }
`;
