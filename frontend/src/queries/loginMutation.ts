import { gql } from 'apollo-boost';

export default gql`
  mutation RunLogin($username: String!, $password: String!) {
    login(name: $username, password: $password) {
      ok
    }
  }
`;
