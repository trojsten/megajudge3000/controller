import c from './c.svg';
import cPlusPlus from './cplusplus.svg';
import java from './java.svg';
import php from './php.svg';
import python from './python.svg';

interface LanguageLogoTable {
  [key: string]: string,
};

const languageLogos : LanguageLogoTable = {
  'text/x-c': c,
  'text/x-c++': cPlusPlus,
  'text/x-java-source': java,
  'text/x-php': php,
  'text/x-python': python,
};

export default languageLogos;
