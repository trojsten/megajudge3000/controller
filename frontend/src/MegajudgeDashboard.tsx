import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from "@material-ui/core/CssBaseline";
import TopBar from "./TopBar";
import SideMenuDrawer from "./SideMenuDrawer";
import Toolbar from "@material-ui/core/Toolbar";
import Container from "@material-ui/core/Container";
import {BrowserRouter as Router, Route} from "react-router-dom";
import Status from "./Status";
import Submits from "./Submits";
import Tasks from "./Tasks";
import Competitions from "./Competitions";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
}));

function MegajudgeDashboard() {
  const classes = useStyles();
  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />
        <TopBar/>
        <SideMenuDrawer />
        <main className={classes.content}>
          <Toolbar />
          <Container maxWidth="lg" className={classes.container}>
            <Route exact path="/">
              <Status />
            </Route>
            <Route path="/submits">
              <Submits />
            </Route>
            <Route path="/tasks">
              <Tasks />
            </Route>
            <Route path="/competitions">
              <Competitions />
            </Route>
          </Container>
        </main>
      </div>
    </Router>
  );
}

export default MegajudgeDashboard;
