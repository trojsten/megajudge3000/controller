/*
The MIT License (MIT)

Copyright (c) 2014 Call-Em-All

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

This file was taken from
https://github.com/mui-org/material-ui/tree/master/docs/src/pages/getting-started/templates/sign-in
and modified for our use-case.
*/

import React, {useContext} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {CircularProgress} from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ErrorIcon from '@material-ui/icons/Error';
import red from "@material-ui/core/colors/red";
import { UserContext } from "./App";
import {useMeQuery, useRunLoginMutation} from "./types/types";
import Backdrop from "@material-ui/core/Backdrop";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  buttonWrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -8,
    marginLeft: -12,
  },
  listErrorItem: {
    color: red[500],
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#3f51b5',
    background: "white"
  },
}));

type SignInErrorProps = {
  key?: number,
  message: string
}

function SignInError(props: SignInErrorProps) {
  const classes = useStyles();
  return (
    <ListItem key={props.key}>
      <ListItemIcon className={classes.listErrorItem}>
        <ErrorIcon />
      </ListItemIcon>
      <ListItemText primary={props.message} className={classes.listErrorItem} />
    </ListItem>
  );
}

function SignInForm() {
  const classes = useStyles();
  const [userNameState, setUserNameState] = React.useState("")
  const [userPasswordState, setUserPasswordState] = React.useState("")
  const [usernameErrors, setUsernameErrors] = React.useState<null | string>(null)
  const [passwordErrors, setPasswordErrors] = React.useState<null | string>(null)
  const [networkError, setNetworkError] = React.useState(false)
  const [runLogin, { loading: loginLoading, error: loginError }] = useRunLoginMutation();
  const userState = useContext(UserContext);

  const updateUserNameState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUserNameState(event.target.value)
  }
  const updatePasswordState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUserPasswordState(event.target.value)
  }

  const logIn = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    if (userNameState.length === 0) {
      setUsernameErrors("This field is required!")
    } else {
      setUsernameErrors(null)
    }
    if (userPasswordState.length === 0) {
      setPasswordErrors("This field is required!")
    } else {
      setPasswordErrors(null)
    }
    setNetworkError(false)

    if (userNameState.length !== 0 && userPasswordState.length !== 0) {
      runLogin({
        variables: {
          username: userNameState,
          password: userPasswordState
        }
      }).then(response => {
        userState.setUserLoggedIn(true)
      }).catch(error => {
        console.log(error.networkError)
        setNetworkError(error.networkError !== null)
      })
    }
  }

  // FIXME This is exposing GraphQL errors directly to the user. It should be reworked for better
  // UX.
  const formErrors = ((loginError?.graphQLErrors === undefined) ? [] : loginError?.graphQLErrors).map(
    (error: { message: string }, index: number) =>
      <SignInError key={index} message={error.message} />
  )

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="User name"
            name="username"
            autoComplete="username"
            autoFocus
            value={userNameState}
            onChange={updateUserNameState}
            error={usernameErrors !== null}
            helperText={usernameErrors}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={updatePasswordState}
            error={passwordErrors !== null}
            helperText={passwordErrors}
          />
          <div className={classes.buttonWrapper}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={logIn}
              disabled={loginLoading}
            >
              Sign In
            </Button>
            {loginLoading && <CircularProgress size={24} className={classes.buttonProgress} />}
          </div>
          <List>
            {formErrors}
            {networkError && <SignInError message="Network error, please try again later." />}
          </List>
        </form>
      </div>
    </Container>
  );
}

export default function SignIn() {
  const classes = useStyles();
  const userState = useContext(UserContext);
  const {loading: loginStatusLoading, data: loginStatus} = useMeQuery();
  if (loginStatus?.me?.username) {
    userState.setUserLoggedIn(true);
  }

  const form = loginStatusLoading ? null : <SignInForm />

  return (
    <div>
      {form}
      <Backdrop className={classes.backdrop} open={loginStatusLoading} >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
}
