import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { onError } from 'apollo-link-error';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from "apollo-cache-inmemory"
import Cookies from 'js-cookie';
import { Options } from 'graphql/utilities/buildClientSchema';
import {UserContextInterface} from "./App";
import { GraphQLError } from 'graphql/error';


const httpLink = new HttpLink({
  uri: `/graphql`,
  credentials: 'same-origin',
});

const csrfMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {} } : {headers: Options}) => ({
    headers: {
      ...headers,
      'X-CSRFTOKEN':  Cookies.get('csrftoken'),
    }
  }));

  return forward(operation);
});

const logoutLink = (userContext: UserContextInterface) =>
  onError(({graphQLErrors, networkError}) => {
    const notLoggedInError = (error: GraphQLError) => {
      return error.extensions?.error === 'NOT-LOGGED-IN'
    }
    if (graphQLErrors && graphQLErrors.some(notLoggedInError)) {
      userContext.setUserLoggedIn(false);
    }
  }
);

export const client = (userContext: UserContextInterface) =>
  new ApolloClient({
    link: ApolloLink.from([
      csrfMiddleware,
      logoutLink(userContext),
      httpLink
    ]),
    cache: new InMemoryCache(),
  });

export default client;
