import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {useCompetitionListQuery} from "./types/types";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
}));


function CompetitionComponent() {
  const {loading, error, data} = useCompetitionListQuery();

  if (loading) {
    return <p>Loading...</p>
  }
  if (error) {
    console.log(error);
    return <p>Error...</p>
  }
  if (data === undefined) {
    return null;
  }
  if (data.competitions.length === 0) {
    return <p>No competitions for you.</p>;
  }

  const competitions = data.competitions.map(
    ({name, webhookUrl}) => (
      <li key={name}>{name} - {webhookUrl}</li>
    )
  );

  return <ul>{competitions}</ul>;
}


function Status() {
  const classes = useStyles();

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={8} lg={9}>
        <Paper className={classes.paper}>
          <CompetitionComponent />
        </Paper>
      </Grid>
      <Grid item xs={12} md={4} lg={3}>
        <Paper className={classes.paper}>
          Y
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          Z
        </Paper>
      </Grid>
    </Grid>
  )
}

export default Status;
