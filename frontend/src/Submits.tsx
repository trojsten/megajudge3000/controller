import React, { useState } from 'react';
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import CheckmarkIcon from '@material-ui/icons/Check';
import CircleIcon from '@material-ui/icons/FiberManualRecord';

import { useSubmitListQuery, SubmitFragment } from './types/types';
import assetLogos from './languageLogos';

function SubmitRow({
  submittedAt, id, priority, assetType, submitAssetsUrl,
  task: {name: taskName, competition: {name: competitionName}},
}: SubmitFragment) {
  const submitDetailsUrl = `/submits/${id}`;
  const submittedAtDateString = (new Date(submittedAt)).toLocaleString();

  return (
    <TableRow key={id}>
      <TableCell>{submittedAtDateString}</TableCell>
      <TableCell align="left">{competitionName}</TableCell>
      <TableCell align="left">{taskName}</TableCell>
      <TableCell align="center"><CircleIcon color="primary" /></TableCell>
      <TableCell align="center">
        <img style={{height: 32}} src={assetLogos[assetType]} alt="Language logo"/>
      </TableCell>
      <TableCell align="center">
        {submitAssetsUrl && <a href={submitAssetsUrl}><CloudDownloadIcon /></a>}
      </TableCell>
      <TableCell align="center"><CheckmarkIcon style={{ color: 'green' }}/></TableCell>
      <TableCell align="center">
        <Link to={submitDetailsUrl}>
          More
        </Link>
      </TableCell>
    </TableRow>
  );
}

function SubmitTable() {
  const DEFAULT_ROWS_PER_PAGE = 16;
  const [state, setState] = useState({
    page: 0,
    rowsPerPage: DEFAULT_ROWS_PER_PAGE,
  });
  const handleChangeRowsPerPage = ({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => {
    setState((previousState) => ({
      ...previousState,
      rowsPerPage: parseInt(value, 10),
    }));
  };
  const handleChangePage = (event: unknown, newPage: number) => {
    setState((previousState) => ({
      ...previousState,
      page: newPage,
    }));
  }
  const { loading, error, data } = useSubmitListQuery({
    variables: {
      // page is numbered from 1 on the server and from 0 in MUI table pagination
      page: state.page + 1,
      rowsPerPage: state.rowsPerPage,
    }
  });

  if (loading) {
    return <p>Loading...</p>
  }
  if (error) {
    console.log(error);
    return <p>Error...</p>
  }
  if (data == null) {
    return null;
  }

  const rows = data.submits?.objects.map(SubmitRow);

  return (
    <TableContainer component={Paper}>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Submitted at</TableCell>
            <TableCell align="left">Competition</TableCell>
            <TableCell align="left">Task</TableCell>
            <TableCell align="center">Priority</TableCell>
            <TableCell align="center">Asset Type</TableCell>
            <TableCell align="center">Asset</TableCell>
            <TableCell align="center">Status</TableCell>
            <TableCell align="center">Details</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[16, 50, 100]}
              count={data.submits?.count || 0}
              rowsPerPage={state.rowsPerPage}
              page={state.page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
}

function Submits() {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <SubmitTable />
      </Grid>
    </Grid>
  );
}

export default Submits;
