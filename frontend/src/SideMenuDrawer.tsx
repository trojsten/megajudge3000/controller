import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import HomeIcon from '@material-ui/icons/Home';
import ListIcon from '@material-ui/icons/List';
import CategoryIcon from '@material-ui/icons/Category';
import FunctionsIcon from '@material-ui/icons/Functions';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    flexShrink: 0,
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
}));

function SideMenuDrawer() {
  const classes = useStyles();
  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>
          <ListItem component={Link} to="/" button key="Status">
            <ListItemIcon><HomeIcon /></ListItemIcon>
            <ListItemText primary="Status" />
          </ListItem>
          <ListItem component={Link} to={'/submits'} button key="Submits">
            <ListItemIcon><ListIcon /></ListItemIcon>
            <ListItemText primary="Submits" />
          </ListItem>
          <ListItem component={Link} to={'/tasks'} button key="Tasks">
            <ListItemIcon><FunctionsIcon /></ListItemIcon>
            <ListItemText primary="Tasks" />
          </ListItem>
          <ListItem component={Link} to={'/competitions'} button key="Users">
            <ListItemIcon><CategoryIcon /></ListItemIcon>
            <ListItemText primary="Competitions" />
          </ListItem>
        </List>
      </div>
    </Drawer>
  );
}

export default SideMenuDrawer;
