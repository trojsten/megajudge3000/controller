FROM python:3.8-alpine3.11 AS base

# Dockerfile explanation
# ----------------------
#
# To get a minimal production-ready image, we are using a multi-stage build.
#
#   - Stage `frontend` builds production frontend bundles and places them to
#     `controller_backend/frontend_build[_static]`.
#   - Stage `python-poetry` installs and configures poetry.
#   - Stage `schema-builder` exports the GraphQL schema used for generating Typescript types for
#     frontend.
#   - Stage `backend-builder` uses Poetry to build a Python wheel containing the backend and
#     previously built frontend static files. `requirements.txt` as also generated so that we
#     install correct versions of our dependencies (that is, the versions which appear in
#     poetry.lock).
#   - Stage `installer` installs all runtime dependencies, project dependencies and the project
#     itself. It installs it to alternate root (`/install`). To install these dependencies, several
#     compliers and development libraries must be present. Instead of cleaning up the image later,
#     we just copy the `/install` folder to the last stage.
#   - The last stage just copies installed Python packages from the `installer` stage, and includes
#     final production image configuration.



FROM base AS python-poetry
# native dependencies are required
# poetry build complains if no git binary is installed
RUN set -ex; \
  apk add --no-cache --update \
    gcc \
    g++ \
    libffi-dev \
    musl-dev \
    openssl-dev ; \
  apk add --no-cache --update git; \
  pip install --no-cache-dir --upgrade setuptools; \
  pip install poetry; \
  poetry config virtualenvs.create false;


FROM python-poetry AS schema-builder
COPY controller_backend/ /build/controller_backend/
# Poetry uses .gitignore to exclude files from the package
COPY pyproject.toml poetry.lock .gitignore manage.py /build/
RUN set -ex; \
  cd build; \
  poetry install --no-dev; \
  poetry run ./manage.py graphql_schema \
    --schema controller_backend.apps.judge.schema.schema \
    --out=/build/graphql-schema.json;


FROM node:12-alpine AS frontend
COPY frontend /build/frontend
COPY --from=schema-builder /build/graphql-schema.json /build/frontend/graphql-schema.json
RUN set -ex; \
  mkdir -p /build/controller_backend; \
  cd /build/frontend; \
  yarn install; \
  yarn build;


FROM python-poetry AS backend-builder
COPY controller_backend/ /build/controller_backend/
# Poetry uses .gitignore to exclude files from the package
COPY pyproject.toml poetry.lock .gitignore manage.py /build/
COPY --from=frontend /build/controller_backend/ /build/controller_backend/
RUN set -ex; \
  cd build; \
  poetry build; \
  poetry export -f requirements.txt -o requirements.txt;


FROM base AS installer
RUN set -ex; \
  apk add --no-cache --update \
    gcc \
    g++ \
    linux-headers \
    musl-dev \
    pcre-dev \
    postgresql-dev; \
  pip install --no-cache-dir --upgrade setuptools;
RUN pip install --no-cache-dir --root /install --no-warn-script-location 'gunicorn' 'psycopg2';
COPY --from=backend-builder /build/dist/*.whl /build/requirements.txt /
RUN set -ex; \
  pip install --root /install --no-warn-script-location -r /requirements.txt; \
  pip install --root /install --no-warn-script-location /*.whl; \
  find /install -depth -type d -name tests -exec rm -rf '{}' +;


FROM base
# TODO this final stage needs further work. This will be done once the application actually does
# something.

# postgresql-libs is a runtime dependency of psycopg2

RUN set -ex; \
  apk add --no-cache --update \
    postgresql-libs; \
  rm -rf /var/cache/apk/*;
COPY --from=installer /install /
COPY docker /

ENV DJANGO_SETTINGS_MODULE=controller_backend.settings.production \
    M3K_BACKEND_DATA_DIR=/var/lib/m3k

ENTRYPOINT ["/usr/local/sbin/entrypoint"]
