# flake8: noqa
import os

from .base import *
from .development_secretkey import *

DEBUG = True

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": os.getenv("DATABASE_ENGINE", "django.db.backends.sqlite3"),
        "NAME": os.getenv(
            "DATABASE_NAME", os.path.join(BASE_DIR, "db.sqlite3")  # noqa: F405
        ),
        "USER": os.getenv("DATABASE_USER"),
        "PASSWORD": os.getenv("DATABASE_PASSWORD"),
        "HOST": os.getenv("DATABASE_HOST"),
        "PORT": os.getenv("DATABASE_PORT", 5432),
    }
}

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")
ALLOWED_HOSTS += ["*"]
