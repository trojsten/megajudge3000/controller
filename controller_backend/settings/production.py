# flake8: noqa
import os

from split_settings.tools import optional, include

from controller_backend.settings.base import *

# !!! Example production settings !!!

# DO NOT FORGET TO SET THIS!!!
# You can use this command to generate the key:
# $ python <<EOF
# from django.core.management.utils import get_random_secret_key
# print(get_random_secret_key())
# EOF
SECRET_KEY = os.getenv("SECRET_KEY")

DATABASES["default"] = {
    "ENGINE": "django.db.backends.postgresql",
    "NAME": os.getenv("DATABASE_NAME", "m3k_production"),
    "USER": os.getenv("DATABASE_USER", "m3k"),
    "PASSWORD": os.getenv("DATABASE_PASSWORD", ""),
    "HOST": os.getenv("DATABASE_HOST", "postgres"),
    "PORT": os.getenv("DATABASE_PORT", "5432"),
}

STATIC_ROOT = os.getenv("M3K_BACKEND_DATA_DIR")

# The admin may place additional custom settings to `/etc/m3k/settings.py` in the Docker container.
# Useful for example for setting deployment specific e-mail or HSTS settings, etc.
include(optional("/etc/m3k/settings.py"))
