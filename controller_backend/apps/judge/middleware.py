from django.middleware.csrf import get_token
from whitenoise.middleware import WhiteNoiseMiddleware


class CookieMonsterWhiteNoiseMiddleware(WhiteNoiseMiddleware):
    """
    Force-set CSRF cookie on requests served by Whitenoise.

    Our `graphql/` API endpoint is behind CSRF protection, but the CSRF cookie was not being set
    by requests served by Whitenoise. Therefore the whole F/E would load (as static assets), and
    API calls would fail. This works around that issue.
    """

    def process_request(self, request):
        get_token(request)
        return super().process_request(request)
