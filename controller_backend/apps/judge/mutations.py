import graphene
from django.contrib.auth import authenticate, login, logout
from graphql import GraphQLError


class LoginMutation(graphene.Mutation):
    class Arguments:
        name = graphene.String(required=True)
        password = graphene.String(required=True)

    ok = graphene.Boolean(required=True)

    @staticmethod
    def mutate(root, info, name, password):
        user = authenticate(request=info.context, username=name, password=password)
        if user is None:
            raise GraphQLError(
                "Invalid user name or password!",
                extensions={"code": "INVALID_CREDENTIALS"},
            )

        login(info.context, user)
        return LoginMutation(ok=True)


class LogoutMutation(graphene.Mutation):
    ok = graphene.Boolean(required=True)

    @staticmethod
    def mutate(root, info):
        logout(info.context)
        return LogoutMutation(ok=True)
