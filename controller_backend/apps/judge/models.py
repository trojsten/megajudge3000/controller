from django.utils.timezone import now
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models


class Competition(models.Model):
    name = models.TextField(primary_key=True)
    api_key = models.CharField(max_length=64)
    webhook_url = models.URLField(blank=True, null=True)
    users = models.ManyToManyField(User, through="CompetitionPermission")

    def __str__(self):
        return self.name


class CompetitionPermission(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    permission_level = models.IntegerField(
        choices=((0, "read-only"), (1, "read-write"),)
    )

    def __str__(self):
        return f"{self.user}@{self.competition}: {self.permission_level}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["competition", "user"],
                name="one_permission_per_user_in_competition",
            )
        ]


class Task(models.Model):
    class TestType(models.IntegerChoices):
        EXECUTE_AND_DIFF = 0, "EXECUTE_AND_DIFF"
        EXECUTE_CUSTOM = 1, "EXECUTE_CUSTOM"

    competition = models.ForeignKey(Competition, on_delete=models.PROTECT)
    name = models.TextField()
    test_assets_url = models.FileField(upload_to="task_assets/")
    test_type = models.IntegerField(choices=TestType.choices)

    def __str__(self):
        return f"{str(self.competition)}/{self.name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["competition", "name"], name="task_name_unique_in_competition"
            )
        ]


class Batch(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="batches")
    name = models.TextField()
    default_time_limit = models.IntegerField(
        help_text="Time limit for all inputs in the batch (in ms)"
    )
    default_memory_limit = models.IntegerField(
        help_text="Memory limit for all inputs in the batch (in MB)"
    )

    def __str__(self):
        return f"{self.task}/{self.name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["task", "name"], name="batch_name_unique_in_task"
            ),
            models.CheckConstraint(
                check=models.Q(default_time_limit__gt=0),
                name="default_time_limit_above_zero",
            ),
            models.CheckConstraint(
                check=models.Q(default_memory_limit__gt=0),
                name="default_memory_limit_above_zero",
            ),
        ]


class BatchConfig(models.Model):
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE, related_name="configs")
    submit_asset_type = models.TextField()
    time_limit = models.IntegerField(
        help_text="Time limit for all inputs in the batch (in ms)"
    )
    memory_limit = models.IntegerField(
        help_text="Memory limit for all inputs in the batch (in MB)"
    )

    def __str__(self):
        return f"{self.batch} for {self.submit_asset_type=}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["batch", "submit_asset_type"], name="unique_asset_type_in_batch"
            ),
            models.CheckConstraint(
                check=models.Q(time_limit__gt=0), name="time_limit_above_zero"
            ),
            models.CheckConstraint(
                check=models.Q(memory_limit__gt=0), name="memory_limit_above_zero"
            ),
        ]


class Priority(models.IntegerChoices):
    MEGA_HIGH = 0
    HIGH = 1
    NORMAL = 2
    LOW = 3
    LOWEST = 4


class Submit(models.Model):
    priority = models.IntegerField(choices=Priority.choices)
    competition_specific_id = models.TextField()
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    asset_type = models.TextField()
    submit_assets_url = models.FileField(upload_to="submit_assets/")
    submitted_at = models.DateTimeField(default=now, editable=False)

    def __str__(self):
        return f"{self.task}/{self.competition_specific_id}"

    def validate_unique(self, exclude=None):
        if (
            len(
                Submit.objects.filter(task__competition=self.task.competition).filter(
                    competition_specific_id=self.competition_specific_id
                )
            )
            != 0
        ):
            raise ValidationError(
                {
                    "competition_specific_id": "Competition-specific ID must be unique for all "
                    "submits within a competition. "
                }
            )
        return super().validate_unique(exclude)


class SubmitTag(models.Model):
    submit = models.ForeignKey(Submit, on_delete=models.CASCADE, related_name="tags")
    name = models.TextField()
    value = models.TextField()

    def __str__(self):
        return f"{self.submit} [{self.name}={self.value}]"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["submit", "name"], name="single_value_per_submit_tag"
            )
        ]
