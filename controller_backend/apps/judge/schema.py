import graphene
from django.contrib.auth import get_user_model
from graphql import GraphQLError

from controller_backend.apps.judge.decorators import login_required
from controller_backend.apps.judge.mutations import LoginMutation, LogoutMutation
from graphene_django.types import DjangoObjectType
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from controller_backend.apps.judge.models import (
    Competition,
    Submit,
    Task,
)


def get_paginator(qs, page_size, page, paginated_type, **kwargs):
    p = Paginator(qs, page_size)
    try:
        page_obj = p.page(page)
    except PageNotAnInteger:
        page_obj = p.page(1)
    except EmptyPage:
        page_obj = p.page(p.num_pages)
    return paginated_type(
        page=page_obj.number,
        pages=p.num_pages,
        count=qs.count(),
        has_next=page_obj.has_next(),
        has_prev=page_obj.has_previous(),
        objects=page_obj.object_list,
        **kwargs,
    )


class CompetitionType(DjangoObjectType):
    class Meta:
        model = Competition


class TaskType(DjangoObjectType):
    class Meta:
        model = Task


class SubmitType(DjangoObjectType):
    class Meta:
        model = Submit


class UserType(DjangoObjectType):
    class Meta:
        model = get_user_model()
        fields = ["email", "username"]
        skip_registry = True


class GraphQLNotLoggedInError(GraphQLError):
    def __init__(
        self,
        nodes=None,
        stack=None,
        source=None,
        positions=None,
        locations=None,
        path=None,
        extensions=None,
    ):
        if extensions is None:
            extensions = dict()
        extensions["error"] = "NOT-LOGGED-IN"
        super().__init__(
            "Login is requred",
            nodes,
            stack,
            source,
            positions,
            locations,
            path,
            extensions,
        )


class MeQuery(graphene.ObjectType):
    me = graphene.Field(UserType)

    @login_required(exc=GraphQLNotLoggedInError)
    def resolve_me(self, info):
        user = info.context.user
        if user.is_authenticated:
            return user
        return None


class SubmitPaginatedType(graphene.ObjectType):
    page = graphene.Int()
    pages = graphene.Int()
    count = graphene.Int()
    has_next = graphene.Boolean()
    has_prev = graphene.Boolean()
    objects = graphene.NonNull(graphene.List(graphene.NonNull(SubmitType)))


class JudgeQuery(graphene.ObjectType):
    competitions = graphene.NonNull(graphene.List(graphene.NonNull(CompetitionType)))
    submits = graphene.Field(
        SubmitPaginatedType, rows_per_page=graphene.Int(), page=graphene.Int()
    )

    @login_required(exc=GraphQLNotLoggedInError)
    def resolve_competitions(self, info, **kwargs):
        return Competition.objects.all()

    @login_required(exc=GraphQLNotLoggedInError)
    def resolve_submits(self, info, rows_per_page, page, **kwargs):
        query_set = Submit.objects.all().order_by("-submitted_at")
        return get_paginator(query_set, rows_per_page, page, SubmitPaginatedType)


class AuthMutation(graphene.ObjectType):
    login = LoginMutation.Field()
    logout = LogoutMutation.Field()


class Query(JudgeQuery, MeQuery, graphene.ObjectType):
    pass


class Mutation(AuthMutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
