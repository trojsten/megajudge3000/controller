import jsonschema
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework import serializers
from rest_framework.fields import empty

from controller_backend.apps.judge.models import (
    Submit,
    Task,
    Batch,
    BatchConfig,
    SubmitTag,
)
from controller_backend.apps.jobs.models import (
    PrepareAssetsJob,
    SubmitTestJob,
    GenericJob,
    BatchResult,
)


class GenericJobResultSerializer(serializers.ModelSerializer):
    execution_log = serializers.JSONField(encoder=DjangoJSONEncoder)
    execution_log_update_position = serializers.IntegerField(required=False)
    status = serializers.CharField(source="get_status_display")

    def __init__(self, instance=None, data=empty, **kwargs):
        schema = {
            "type": "array",
            "items": {
                "type": "object",
                "required": ["stream", "line"],
                "properties": {
                    "stream": {"type": "string", "enum": ["STDOUT", "STDERR"]},
                    "line": {"type": "string"},
                },
            },
        }
        cls = jsonschema.validators.validator_for(schema)
        cls.check_schema(schema)
        self.execution_log_validator = cls(schema)
        super().__init__(instance, data, **kwargs)

    class Meta:
        model = GenericJob
        fields = [
            "status",
            "execution_log",
            "execution_log_update_position",
            "execution_duration",
        ]

    def validate_status(self, status):
        try:
            status = {x.name: x.value for x in GenericJob.Status}[status]
        except KeyError:
            raise serializers.ValidationError(f"Unknown status {status}")
        if not self.partial:
            if status not in GenericJob.terminating_states:
                raise serializers.ValidationError(
                    "On final job results update, this field must be set to one of the terminating "
                    "states."
                )
        else:
            if status in GenericJob.terminating_states:
                raise serializers.ValidationError(
                    "Partial job update can't set job status to a terminating state."
                )
        return status

    def validate_execution_log(self, execution_log):
        try:
            self.execution_log_validator.validate(execution_log)
            return execution_log
        except jsonschema.ValidationError as e:
            raise serializers.ValidationError(
                f"Invalid schema: {e.message} in "
                f"execution_log{''.join([f'[{x}]' for x in e.absolute_path])}"
            )

    def validate_execution_log_update_position(self, pos):
        if not self.partial:
            raise serializers.ValidationError(
                "Field 'execution_log_update_position' can't be present for final updates"
            )
        if pos > (max_pos := len(self.instance.get_execution_log())) or pos < -1:
            raise serializers.ValidationError(
                f"Invalid position. Valid positions are -1 and 0-{max_pos}"
            )
        return pos

    def update(self, instance, validated_data):
        # Ensure that this is called from within an atomic transaction!
        try:
            # get_status_display is actually the value of a status field
            instance.status = validated_data["get_status_display"]
        except KeyError:
            pass

        try:
            instance.execution_duration = validated_data["execution_duration"]
        except KeyError:
            pass

        try:
            if "execution_log_update_position" not in validated_data:
                instance.set_execution_log(validated_data["execution_log"])
            else:
                # Allowable values for validated_data["execution_log_update_position"] are
                # -1, 0-len(instance.get_execution_log()). -1 means "append", other values mean
                # "replace from position x".
                if (pos := validated_data["execution_log_update_position"]) >= 0:
                    log = instance.get_execution_log()[:pos]
                else:
                    log = instance.get_execution_log()
                instance.set_execution_log(log + validated_data["execution_log"])
        except KeyError:
            pass

        instance.save()
        return instance


class PrepareAssetsJobSerializer(serializers.ModelSerializer):
    job_type = serializers.ReadOnlyField(default="PREPARE_ASSETS")

    class Meta:
        model = PrepareAssetsJob
        fields = ["id", "job_type", "original_asset", "for_task"]


class BatchConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = BatchConfig
        fields = ["submit_asset_type", "time_limit", "memory_limit"]


class BatchSerializer(serializers.ModelSerializer):
    configs = BatchConfigSerializer(read_only=True, many=True)

    class Meta:
        model = Batch
        fields = ["id", "name", "default_time_limit", "default_memory_limit", "configs"]


class TaskSerializer(serializers.ModelSerializer):
    batches = BatchSerializer(read_only=True, many=True)
    test_type = serializers.CharField(source="get_test_type_display")

    class Meta:
        model = Task
        fields = ["test_assets_url", "test_type", "batches"]


class SubmitTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmitTag
        fields = ["name", "value"]


class SubmitSerializer(serializers.ModelSerializer):
    task = TaskSerializer(read_only=True)
    tags = SubmitTagSerializer(read_only=True, many=True)

    class Meta:
        model = Submit
        fields = [
            "competition_specific_id",
            "task",
            "asset_type",
            "submit_assets_url",
            "tags",
        ]


class SubmitTestJobSerializer(serializers.ModelSerializer):
    job_type = serializers.ReadOnlyField(default="TEST_SUBMIT")
    submit = SubmitSerializer(read_only=True)

    class Meta:
        model = SubmitTestJob
        fields = ["id", "job_type", "submit"]


class BatchResultSerializer(serializers.ModelSerializer):
    protocol = serializers.JSONField()

    def __init__(self, instance=None, data=empty, **kwargs):
        schema = {
            "type": "array",
            "items": {
                "type": "object",
                "required": ["name", "result", "time"],
                "properties": {
                    "name": {"type": "string", "minLength": 1},
                    "result": {"type": "string", "minLength": 1},
                    "time": {"type": "number", "minimum": 0},
                },
            },
        }
        cls = jsonschema.validators.validator_for(schema)
        cls.check_schema(schema)
        self.protocol_validator = cls(schema)
        super().__init__(instance, data, **kwargs)

    class Meta:
        model = BatchResult
        fields = ["for_batch", "protocol", "result"]

    def validate_protocol(self, protocol):
        try:
            self.protocol_validator.validate(protocol)
            return protocol
        except jsonschema.ValidationError as e:
            raise serializers.ValidationError(
                f"Invalid schema: {e.message} in "
                f"protocol{''.join([f'[{x}]' for x in e.absolute_path])}"
            )


class SubmitTestJobResultSerializer(GenericJobResultSerializer):
    batch_results = BatchResultSerializer(many=True)

    class Meta(GenericJobResultSerializer.Meta):
        model = SubmitTestJob
        fields = GenericJobResultSerializer.Meta.fields + [
            "summary_result",
            "batch_results",
        ]

    def validate_batch_results(self, batch_results):
        ids_of_batches_with_resuts = set()
        for result in batch_results:
            if result["for_batch"].id in ids_of_batches_with_resuts:
                raise serializers.ValidationError(
                    f"Duplicate batch result ID {result['for_batch'].id}"
                )
            ids_of_batches_with_resuts.add(result["for_batch"].id)

        ids_of_all_batches_in_task = set(
            self.instance.submit.task.batches.values_list("id", flat=True)
        )

        foreign_batches = ids_of_batches_with_resuts - ids_of_all_batches_in_task
        if foreign_batches:
            raise serializers.ValidationError(
                f"Result update contains batches not associated with the task under test (invalid "
                f"ids {foreign_batches})."
            )

        if not self.partial:
            # non-partial update must contain results for all batches
            if ids_of_batches_with_resuts != ids_of_all_batches_in_task:
                raise serializers.ValidationError(
                    f"Final result update must contain results for all batches in a task (that is"
                    f"batches with IDs {ids_of_all_batches_in_task})."
                )

        return batch_results

    def update(self, instance, validated_data):
        instance.summary_result = validated_data.get(
            "summary_result", instance.summary_result
        )
        instance.save()

        for batch_result in (
            validated_data.pop("batch_results")
            if "batch_results" in validated_data
            else []
        ):
            try:
                br_object = instance.batch_results.filter(
                    for_batch=batch_result["for_batch"]
                )[0]
            except IndexError:
                # The batch result does not exist and must be created
                br_object = BatchResult(
                    for_batch=batch_result["for_batch"], submit_test_job=instance
                )

            br_object.set_protocol(batch_result["protocol"])
            br_object.result = batch_result["result"]
            br_object.save()

        super().update(instance, validated_data)
        return instance
