import json

from django.db import models
from django.db.models import CASCADE, PROTECT
from django.db.models.signals import post_save
from django.dispatch import receiver
from model_utils.managers import InheritanceManager

from rest_framework.authtoken.models import Token

from controller_backend.apps.judge.models import Priority, Task, Batch, Submit


class Worker(models.Model):
    stopped = models.BooleanField(default=False)
    last_contact = models.DateTimeField(blank=True, null=True)
    is_active = models.BooleanField(default=True)

    @property
    def is_authenticated(self):
        """
        Always return True, since this is not an anonymous workers. See
        https://docs.djangoproject.com/en/3.0/ref/contrib/auth/#django.contrib.auth.models.User.is_authenticated
        for for more info.
        """
        return True


class WorkerToken(Token):
    """
    Token for authenticating workers.

    The DNF's Token model authenticates model `settings.AUTH_USER_MODEL`, which is a user. We wish
    to authenticate Workers (without changing the AUTH_USER_MODEL setting), so we override the user
    field and use our custom subclass of TokenAuthentication.
    """

    # It is a requirement of DRF that this field is called user, despite the fact that it refers to
    # a Worker.
    user = models.OneToOneField(
        Worker,
        related_name="auth_token",
        on_delete=models.CASCADE,
        verbose_name="Worker",
    )


@receiver(post_save, sender=Worker)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        WorkerToken.objects.create(user=instance)


class GenericJob(models.Model):
    class Status(models.IntegerChoices):
        PENDING = 0, "PENDING"
        PREPARING = 1, "PREPARING"
        RUNNING = 2, "RUNNING"
        DONE = 3, "DONE"
        CANCELLED = 4, "CANCELLED"
        FAILED = 5, "FAILED"

    terminating_states = {Status.DONE, Status.CANCELLED, Status.FAILED}

    priority = models.IntegerField(choices=Priority.choices)
    created_at = models.DateTimeField()
    scheduled_for = models.DateTimeField(null=True)
    worker = models.ForeignKey(Worker, on_delete=models.PROTECT, null=True)

    status = models.IntegerField(choices=Status.choices)
    execution_log = models.TextField(blank=True, null=True)
    execution_duration = models.DurationField(blank=True, null=True)

    objects = InheritanceManager()

    @property
    def is_finished(self):
        return self.status in GenericJob.terminating_states

    # Use of native JSONField would help here. See the README.
    def set_execution_log(self, new_execution_log):
        self.execution_log = json.dumps(new_execution_log)

    def get_execution_log(self):
        return json.loads(self.execution_log)

    def reset_results(self):
        """
        Reset the job results.

        This function removes all results of this job and returns it to the PENDING state.
        """
        self.execution_log = None
        self.execution_duration = None
        self.status = GenericJob.Status.PENDING
        self.worker = None


class PrepareAssetsJob(GenericJob):
    for_task = models.ForeignKey(Task, on_delete=CASCADE)
    original_asset = models.FileField(upload_to="asset_prep_source/")


class SubmitTestJob(GenericJob):
    submit = models.ForeignKey(Submit, on_delete=CASCADE)
    summary_result = models.TextField(blank=True, null=True)

    def reset_results(self):
        super().reset_results()
        self.summary_result = None
        self.batch_results.all().delete()


class BatchResult(models.Model):
    for_batch = models.ForeignKey(Batch, on_delete=PROTECT)
    submit_test_job = models.ForeignKey(
        SubmitTestJob, on_delete=models.CASCADE, related_name="batch_results"
    )
    protocol = models.TextField(blank=True, null=True)
    result = models.TextField(blank=True, null=True)

    # Use of native JSONField would help here. See the README.
    def set_protocol(self, new_protocol):
        self.protocol = json.dumps(new_protocol)

    def get_protocol(self):
        return json.loads(self.protocol)
