from datetime import timedelta
from unittest.mock import patch

from django.urls import include, path
from django.utils import timezone
from rest_framework import status
from rest_framework.test import URLPatternsTestCase, APITransactionTestCase

from controller_backend.apps.jobs.conf import settings
from controller_backend.apps.judge.models import (
    Competition,
    Task,
    Priority,
    Submit,
    Batch,
)
from controller_backend.apps.jobs.models import (
    Worker,
    PrepareAssetsJob,
    GenericJob,
    SubmitTestJob,
    BatchResult,
)


class JobSchedulingTestCase(APITransactionTestCase, URLPatternsTestCase):
    urlpatterns = [path("worker-api/", include("controller_backend.apps.jobs.urls"))]

    def setUp(self) -> None:
        self.active_workers = [Worker.objects.create() for _ in range(3)]
        self.worker_inactive = Worker.objects.create(is_active=False)
        self.worker_stopped = Worker.objects.create(stopped=True)

        ksp = Competition.objects.create(name="ksp")
        self.tasks = [
            Task.objects.create(
                name="task-1", competition=ksp, test_type=Task.TestType.EXECUTE_AND_DIFF
            ),
            Task.objects.create(
                name="task-2", competition=ksp, test_type=Task.TestType.EXECUTE_AND_DIFF
            ),
            Task.objects.create(
                name="task-3", competition=ksp, test_type=Task.TestType.EXECUTE_AND_DIFF
            ),
        ]

        self.job = PrepareAssetsJob.objects.create(
            priority=Priority.NORMAL,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            for_task=self.tasks[0],
            original_asset="asset_prep_source/22cb123d-f04d-44f5-9e81-bd5119950d88.zip",
        )

    def test_unautheticated_worker_is_unauthorized(self):
        response = self.client.get("/worker-api/jobs/next", format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_inactive_worker_is_unauthorized(self):
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker_inactive.auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_stopped_worker_wont_get_jobs(self):
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker_stopped.auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_worker_gets_job(self):
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["job_type"], "PREPARE_ASSETS")
        self.assertEqual(response.data["for_task"], self.job.for_task.id)

        self.job.refresh_from_db()

        self.assertGreater(self.job.scheduled_for, timezone.now())
        self.assertEqual(self.job.worker, self.active_workers[0])
        self.assertEqual(self.job.status, GenericJob.Status.PREPARING)

    def test_previously_unfinished_job_is_reset_on_next_request(self):
        job2 = PrepareAssetsJob.objects.create(
            priority=Priority.NORMAL,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            for_task=self.tasks[1],
            original_asset="asset_prep_source/22cb123d-f04d-44f5-9e81-bd5119950d88.zip",
        )

        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.job.refresh_from_db()
        self.assertEqual(self.job.status, GenericJob.Status.PREPARING)

        # This request should reset the already-assigned job, the new job we get should be job2
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["for_task"], job2.for_task.id)

        self.job.refresh_from_db()
        self.assertEqual(self.job.status, GenericJob.Status.PENDING)
        self.assertEqual(self.job.worker, None)

    def test_already_scheduled_job_wont_be_reassinged(self):
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_2 = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[1].auth_token}",
        )
        self.assertEqual(response_2.status_code, status.HTTP_204_NO_CONTENT)

    def test_unfinished_job_is_rescheduled(self):
        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        now = timezone.now()
        with patch(
            "django.utils.timezone.now",
            new=lambda: now + (settings.JOB_RESCHEDULE_TIMEDELTA * 2),
        ):
            response = self.client.get(
                "/worker-api/jobs/next",
                format="json",
                HTTP_AUTHORIZATION=f"Token {self.active_workers[1].auth_token}",
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_jobs_are_scheduled_in_right_order(self):
        hp_job_sooner = PrepareAssetsJob.objects.create(
            priority=Priority.HIGH,
            created_at=timezone.now() - timedelta(minutes=1),
            scheduled_for=timezone.now() - timedelta(minutes=1),
            status=GenericJob.Status.PENDING,
            for_task=self.tasks[1],
            original_asset="asset_prep_source/22cb123d-f04d-44f5-9e81-bd5119950d88.zip",
        )
        hp_job_later = PrepareAssetsJob.objects.create(
            priority=Priority.HIGH,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            for_task=self.tasks[2],
            original_asset="asset_prep_source/22cb123d-f04d-44f5-9e81-bd5119950d88.zip",
        )

        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["for_task"], hp_job_sooner.for_task.id)

        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[1].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["for_task"], hp_job_later.for_task.id)

        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[2].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["for_task"], self.job.for_task.id)

    def test_worker_gets_submit_test_job(self):
        self.job.scheduled_for = None
        self.job.save()

        submit = Submit.objects.create(
            priority=Priority.NORMAL,
            competition_specific_id="S-0001",
            task=self.tasks[0],
            asset_type="text/x-c++",
            submit_assets_url="submit_assets/5a983c89-e664-40c1-9513-9ff57cab38b4.zip",
        )

        SubmitTestJob.objects.create(
            priority=Priority.NORMAL,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            submit=submit,
        )

        response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.active_workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["submit"]["competition_specific_id"],
            submit.competition_specific_id,
        )


class ResultSubmissionTestCase(APITransactionTestCase, URLPatternsTestCase):
    urlpatterns = [path("worker-api/", include("controller_backend.apps.jobs.urls"))]

    def setUp(self) -> None:
        self.workers = list([Worker.objects.create() for _ in range(2)])

        ksp = Competition.objects.create(name="ksp")
        self.tasks = [
            Task.objects.create(
                name="task-0", competition=ksp, test_type=Task.TestType.EXECUTE_AND_DIFF
            ),
        ]

        self.job = PrepareAssetsJob.objects.create(
            priority=Priority.NORMAL,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            for_task=self.tasks[0],
            original_asset="asset_prep_source/22cb123d-f04d-44f5-9e81-bd5119950d88.zip",
        )

    def _request_job(self):
        return self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )

    def test_unauthenticated_worker_is_unauthorized(self):
        response = self.client.patch("/worker-api/jobs/0/results", format="json",)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unknown_job_is_not_found(self):
        response = self.client.post(
            "/worker-api/jobs/4247/results",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_results_update_by_unassigned_worker_is_forbidden(self):
        get_job_response = self._request_job()
        self.assertEqual(get_job_response.status_code, status.HTTP_200_OK)

        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[1].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_403_FORBIDDEN)

    def test_final_results_update_prepareassetsjob(self):
        get_job_response = self._request_job()
        self.assertEqual(get_job_response.status_code, status.HTTP_200_OK)

        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_204_NO_CONTENT)
        self.job.refresh_from_db()
        # There are separate tests for setting and updating job log
        self.assertEqual(self.job.status, 3)
        self.assertEqual(self.job.execution_duration, timedelta(seconds=10))

    def test_final_results_update_status_cant_be_nonterminal(self):
        get_job_response = self._request_job()
        self.assertEqual(get_job_response.status_code, status.HTTP_200_OK)

        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": 2,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.job.refresh_from_db()
        # Job status should be unchanged
        self.assertEqual(self.job.status, 1)

    def test_finalized_job_results_cant_be_updated(self):
        get_job_response = self._request_job()
        self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:20",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.CANCELLED.name,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:20",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_423_LOCKED)

    def test_bad_execution_log_schema_is_invalid_request(self):
        get_job_response = self._request_job()
        self.assertEqual(get_job_response.status_code, status.HTTP_200_OK)

        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "badstream", "line": "Hello world"}],
                "execution_duration": "00:00:10",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)
        post_result_response = self.client.post(
            f"/worker-api/jobs/{get_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "STDOUT"}],
                "execution_duration": "00:00:10",
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.workers[0].auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)


class SubmitTestJobTestCase(APITransactionTestCase):
    def setUp(self) -> None:
        self.worker = Worker.objects.create()

        ksp = Competition.objects.create(name="ksp")
        self.tasks = [
            Task.objects.create(
                name=f"task={x}",
                competition=ksp,
                test_type=Task.TestType.EXECUTE_AND_DIFF,
            )
            for x in range(2)
        ]

        for task in self.tasks:
            [
                Batch.objects.create(
                    task=task,
                    name=f"batch-{x}",
                    default_time_limit=1000,
                    default_memory_limit=1024,
                )
                for x in range(3)
            ]

        self.submit = Submit.objects.create(
            priority=Priority.NORMAL,
            competition_specific_id="submit-0",
            task=self.tasks[0],
            asset_type="text/x-c++",
            submit_assets_url="submit_assets/ksp/task-0/submit-0.zip",
        )

        self.job = SubmitTestJob.objects.create(
            priority=Priority.NORMAL,
            created_at=timezone.now(),
            scheduled_for=timezone.now(),
            status=GenericJob.Status.PENDING,
            submit=self.submit,
        )

        self.request_job_response = self.client.get(
            "/worker-api/jobs/next",
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )

        self.job.refresh_from_db()


class SubmitTestJobResultSubmissionTestCase(SubmitTestJobTestCase, URLPatternsTestCase):
    urlpatterns = [path("worker-api/", include("controller_backend.apps.jobs.urls"))]

    def test_complete_submit_test_job_result_submission(self):
        protocol = [{"name": "sample.00.in", "result": "OK", "time": 1.320}]
        post_result_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": protocol,
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ]
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_204_NO_CONTENT)
        for batch in self.request_job_response.data["submit"]["task"]["batches"]:
            batch_result = BatchResult.objects.filter(for_batch=batch["id"])[0]
            self.assertEqual(batch_result.result, f"WA-{batch['id']}")
            self.assertEqual(batch_result.get_protocol(), protocol)
        self.job.refresh_from_db()
        self.assertEqual(self.job.summary_result, "WA")

    def test_submission_with_invalid_protocol_is_invalid(self):
        post_result_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": GenericJob.Status.DONE.name,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": [{"name": "sample.00.in", "time": 1.320}],
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ]
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_final_test_job_submission_with_missing_batches_is_invalid(self):
        post_result_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": 3,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": "Hello world",
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ][0:1]
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_test_job_submission_with_duplicate_batches_is_invalid(self):
        post_result_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": 3,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": "Hello world",
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ]
                ]
                + [
                    {
                        "for_batch": self.request_job_response.data["submit"]["task"][
                            "batches"
                        ][0]["id"],
                        "protocol": "Hello world",
                        "result": "WA",
                    }
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_test_job_submission_with_foreign_batches_is_invalid(self):
        post_result_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": 3,
                "execution_log": [{"stream": "STDOUT", "line": "Hello world"}],
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": "Hello world",
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ]
                ]
                + [
                    {
                        "for_batch": self.tasks[1].batches.all()[0].id,  # foreign batch
                        "protocol": "Hello world",
                        "result": "WA",
                    }
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(post_result_response.status_code, status.HTTP_400_BAD_REQUEST)


class SubmitTestJobPartialResultsUpdateTestCase(
    SubmitTestJobTestCase, URLPatternsTestCase
):
    urlpatterns = [path("worker-api/", include("controller_backend.apps.jobs.urls"))]

    def test_partial_job_status_update(self):
        patch_result_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {"status": GenericJob.Status.RUNNING.name},
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(patch_result_response.status_code, status.HTTP_204_NO_CONTENT)
        self.job.refresh_from_db()
        self.assertEqual(self.job.status, GenericJob.Status.RUNNING)

    def test_partial_job_status_cant_be_terminal(self):
        patch_result_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {"status": GenericJob.Status.FAILED.name},
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(patch_result_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_partial_batch_update(self):
        patch_result_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "batch_results": [
                    {
                        "for_batch": self.request_job_response.data["submit"]["task"][
                            "batches"
                        ][0]["id"],
                        "protocol": [
                            {"name": "sample.00.in", "result": "OK", "time": 0}
                        ],
                        "result": "OK",
                    }
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(patch_result_response.status_code, status.HTTP_204_NO_CONTENT)
        self.job.refresh_from_db()
        self.assertEqual(
            self.job.batch_results.filter(
                for_batch=self.request_job_response.data["submit"]["task"]["batches"][
                    0
                ]["id"]
            )[0].result,
            "OK",
        )

    def test_job_log_update(self):
        initial_log = [
            {"stream": "STDOUT", "line": "Hello world"},
            {"stream": "STDOUT", "line": "Line 2"},
            {"stream": "STDOUT", "line": "Line 3"},
        ]
        initial_log_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {"execution_log": initial_log},
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(initial_log_response.status_code, status.HTTP_204_NO_CONTENT)
        self.job.refresh_from_db()
        self.assertEqual(initial_log, self.job.get_execution_log())

    def _add_initial_log_to_job(self):
        initial_log = [
            {"stream": "STDOUT", "line": "Hello world"},
            {"stream": "STDOUT", "line": "Line 2"},
            {"stream": "STDOUT", "line": "Line 3"},
        ]
        self.job.set_execution_log(initial_log)
        self.job.save()
        return initial_log

    def test_partial_job_log_update(self):
        initial_log = self._add_initial_log_to_job()
        log_update = [
            {"stream": "STDOUT", "line": "New line 3"},
            {"stream": "STDOUT", "line": "New line 4"},
        ]
        log_update_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {"execution_log": log_update, "execution_log_update_position": 2},
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(log_update_response.status_code, status.HTTP_204_NO_CONTENT)
        self.job.refresh_from_db()
        self.assertEqual(initial_log[:2] + log_update, self.job.get_execution_log())

    def test_partial_job_log_update_with_post_request_should_fail(self):
        self._add_initial_log_to_job()
        log_update = [
            {"stream": "STDOUT", "line": "New line 3"},
            {"stream": "STDOUT", "line": "New line 4"},
        ]
        log_update_response = self.client.post(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "status": 3,
                "execution_log": log_update,
                "execution_log_update_position": 2,
                "execution_duration": "00:00:10",
                "summary_result": "WA",
                "batch_results": [
                    {
                        "for_batch": batch["id"],
                        "protocol": "Hello world",
                        "result": f"WA-{batch['id']}",
                    }
                    for batch in self.request_job_response.data["submit"]["task"][
                        "batches"
                    ]
                ],
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(log_update_response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_job_log_update_with_incorrect_pos_should_fail(self):
        self._add_initial_log_to_job()
        log_update = [
            {"stream": "STDOUT", "line": "New line 3"},
            {"stream": "STDOUT", "line": "New line 4"},
        ]
        log_update_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "execution_log": log_update,
                "execution_log_update_position": 10,  # Out-of-range
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(log_update_response.status_code, status.HTTP_400_BAD_REQUEST)
        log_update_response = self.client.patch(
            f"/worker-api/jobs/{self.request_job_response.data['id']}/results",
            {
                "execution_log": log_update,
                "execution_log_update_position": -3,  # The only negative value allowed is -1
            },
            format="json",
            HTTP_AUTHORIZATION=f"Token {self.worker.auth_token}",
        )
        self.assertEqual(log_update_response.status_code, status.HTTP_400_BAD_REQUEST)
