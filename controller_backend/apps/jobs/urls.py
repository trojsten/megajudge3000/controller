from django.urls import path

from controller_backend.apps.jobs import views

urlpatterns = [
    path("jobs/next", views.NextJobView.as_view()),
    path("jobs/<int:jid>/results", views.JobResutsView.as_view()),
]
