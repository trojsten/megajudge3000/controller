from datetime import timedelta

from django import conf
from django.core.exceptions import ImproperlyConfigured


class Settings:
    def __init__(self):  # noqa: C901
        if not isinstance(self.JOB_RESCHEDULE_TIMEDELTA, timedelta):
            raise ImproperlyConfigured(
                "The JOB_RESCHEDULE_TIMEDELTA must be a datetime.timedelta " "object."
            )

    @property
    def JOB_RESCHEDULE_TIMEDELTA(self):  # noqa: N802
        return getattr(conf.settings, "JOB_RESCHEDULE_TIMEDELTA", None)


settings = Settings()
