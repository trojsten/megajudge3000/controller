from django.db import transaction
from django.utils import timezone
from rest_framework import authentication, status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from controller_backend.apps.jobs.conf import settings
from controller_backend.apps.jobs.serializers import (
    PrepareAssetsJobSerializer,
    SubmitTestJobSerializer,
    SubmitTestJobResultSerializer,
    GenericJobResultSerializer,
)
from controller_backend.apps.jobs.models import (
    WorkerToken,
    GenericJob,
    PrepareAssetsJob,
    SubmitTestJob,
)


class WorkerTokenAuthentication(authentication.TokenAuthentication):
    model = WorkerToken


class NextJobView(APIView):
    """
    Get the next enqueued job.

    The Worker uses this endpoint to request some work. If there is any job enqueued, it will be
    returned and assigned to the requesting worker.
    """

    authentication_classes = [WorkerTokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        # !!! This function does not have automated tests for race-conditions !!!
        # Due to difficulties with writing automated tests, there is no test that checks whether
        # rows are properly locked and skipped when assigning work.
        # If you touch something related to transactions or row locking, please manually verify that
        # this function is still immune to race-conditions.
        #
        # You follow these example steps (replace stuff in <> as needed):
        # Paste this piece of code before candidate_job.save():
        #
        # if "X-Sleep" in request.headers:
        #     import time
        #     time.sleep(int(request.headers["X-Sleep"]))
        #
        # Load the appropriate fixtures so that a single job is pending, and run 2 API requests:
        #
        # curl -H "Authorization: Token <whatever>" -H "X-Sleep: 240" -v <url>/worker-api/jobs/next
        # curl -H "Authorization: Token <whatever>" -v <url>/worker-api/jobs/next
        #
        # The first curl call should block, giving you time to execute the second curl call,
        # simulating a race condition.
        # Expected status code of the second call is 204 - No Content - since the job is locked by
        # the first call.
        # After the first call finishes sleeping, it should return 200 - OK and JSON structure
        # describing the job.
        #
        # To simulate this from automated tests, we would have to create 2 separate connections
        # to a database and launch 2 separate transactions - I haven't found a simple maintainable
        # way to do that.
        if request.user.stopped:
            # Stopped workers wont get any jobs
            return Response(None, status=status.HTTP_204_NO_CONTENT)

        # Check if the worker has another job assigned that it did not finish
        try:
            incomplete_job = (
                GenericJob.objects.filter(worker=request.user)
                .exclude(status__in=GenericJob.terminating_states)
                .select_subclasses()[0]
            )
            incomplete_job.reset_results()
            incomplete_job.scheduled_for = timezone.now()
            incomplete_job.save()
        except IndexError:
            pass

        with transaction.atomic():
            try:
                candidate_job = (
                    GenericJob.objects.select_for_update(of=("self",), skip_locked=True)
                    .filter(scheduled_for__lte=timezone.now())
                    .order_by("priority", "scheduled_for")
                    .select_subclasses()[0]
                )
            except IndexError:
                return Response(None, status=status.HTTP_204_NO_CONTENT)

            candidate_job.reset_results()

            if isinstance(candidate_job, PrepareAssetsJob):
                serializer = PrepareAssetsJobSerializer(candidate_job)
            else:
                serializer = SubmitTestJobSerializer(candidate_job)

            candidate_job.worker = request.user
            candidate_job.status = GenericJob.Status.PREPARING
            candidate_job.scheduled_for = (
                timezone.now() + settings.JOB_RESCHEDULE_TIMEDELTA
            )

            candidate_job.save()

            return Response(serializer.data, status=status.HTTP_200_OK)


class HasAssignedJob(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.worker == request.user


class JobResutsView(APIView):
    """
    Update job execution results.

    This can either be a partial update, or a final update when the job execution finishes.
    """

    authentication_classes = [WorkerTokenAuthentication]
    permission_classes = [permissions.IsAuthenticated, HasAssignedJob]

    @transaction.atomic
    def _submit_result(self, request, jid, partial):
        try:
            job = GenericJob.objects.filter(id=jid).select_subclasses().all()[0]
        except IndexError:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

        self.check_object_permissions(request, job)
        if job.is_finished:
            return Response(None, status=status.HTTP_423_LOCKED)

        if isinstance(job, SubmitTestJob):
            serializer = SubmitTestJobResultSerializer(
                job, data=request.data, partial=partial
            )
        else:
            serializer = GenericJobResultSerializer(
                job, data=request.data, partial=partial
            )

        if serializer.is_valid():
            serializer.save()
            return Response(None, status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, jid=None):
        return self._submit_result(request, jid, partial=True)

    def post(self, request, jid=None):
        return self._submit_result(request, jid, partial=False)
